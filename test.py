import os


image = "image"
ortho = "odm_orthophoto"
texture = "odm_texturing"
def main():
    os.system("docker pull opendronemap/odm")
    os.system(f"docker run -i \ -v '$(pwd)/images:{image}' \ -v '$(pwd)/odm_orthophoto:{ortho}' \ -v '$(pwd)/odm_texturing:{texture}' \ opendronemap/opendronemap")

if __name__ == '__main__':
    main()
